from core.settings.base import *


ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': env.db()
}
